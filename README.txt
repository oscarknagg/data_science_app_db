##############################
### Instructions for setup ###
##############################

1. Download project.
2. Create a virtual python environment with the requirements given in the root directory.
3. Navigate to /threatIntelligence and give the command python manage.py runserver
4. Hopefully this should work


########################
### Summary of pages ###
########################


/androguard:

Offers two methods of submitting an app for analysis by androguard.
1. Upload an app from your local computer.
2. Give bucketname/path/to/file of an app in an s3 bucket.
These will save the feature representation of the app to the database.


/vtkoodousscraper:

Lets you start and stop collecting apps from Koodous and labels from Virustotal.


