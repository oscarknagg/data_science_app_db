from django.shortcuts import render


def index(request):
    response = {}
    if request.method =="GET":
        return render(request, 'machineLearning/index.html', response)
    # else:
    #     if 'calculate' in request.POST.keys():
    #         truncate_mldata()
    #         generate_feature_reps()
    #     else:
    #         ml_main()
    
    return render(request, 'machineLearning/index.html', response)