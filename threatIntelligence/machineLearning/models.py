from __future__ import unicode_literals
from django.db import models
from django.contrib.postgres.fields import ArrayField
from mobileappanalysis.models import apk as apkmodel

# Create your models here.

class flat_app_features(models.Model):
    apk = models.ForeignKey(apkmodel, on_delete=models.CASCADE,null=True)
    category =  models.TextField()
    feature = models.TextField()
    
    def __str__(self):
        return "%s , %s , %s " % (self.apk , self.feature , self.category)


class observed_features(models.Model):
    category =  models.TextField()
    feature = models.TextField()
    
    def __str__(self):
        return "%s , %s " % (self.feature , self.category)


class vector_app_features(models.Model):
    apk = models.ForeignKey(apkmodel, on_delete=models.CASCADE,null=True)
    bag = ArrayField(models.TextField())
    vector = ArrayField(models.IntegerField())
    
    def __str__(self):
        return "%s" % (self.apk)