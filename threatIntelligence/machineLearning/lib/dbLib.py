from django.db import connection
from mobileappanalysis.models import apk, virustotal, koodous
from machineLearning.models import flat_app_features, vector_app_features, observed_features
import time

def recreate_feature_lists():
    print 'recreating feature lists...'
    apk_ids = apk.objects.all().values_list('id',flat=True).order_by('id')
    
    n = len(apk_ids)
    for i in apk_ids:
        app_features = flat_app_features.objects.filter(apk_id=i).values_list('feature',flat=True)
        print i,n,len(app_features)
        try:
            vector_app_features_dbEntry = vector_app_features.objects.get(apk_id=i)
        except vector_app_features.DoesNotExist:
            vector_app_features_dbEntry = vector_app_features(apk_id=i,bag=app_features,vector=[])
            vector_app_features_dbEntry.apk_id = i
            vector_app_features_dbEntry.bag = list(app_features)
            vector_app_features_dbEntry.vector = []
            vector_app_features_dbEntry.save()
            
        vector_app_features_dbEntry.bag = list(app_features)
        vector_app_features_dbEntry.save()

def truncate_mldata():
    print 'Truncating vector representations...'
    cursor = connection.cursor()
    cursor.execute('TRUNCATE "machineLearning_observed_features" RESTART IDENTITY;')


def generate_feature_reps():
#    recreate_feature_lists()
#    return None
    
    print 'Generating machine learning tables from raw data...'
    create_observed_features_table()
#    calculate_feature_vectors()


def create_observed_features_table():
    print 'Creating observed_features_table...'
    start = time.time()
    distinct_features = flat_app_features.objects.all().values_list('category','feature').distinct()
    observed_features.objects.bulk_create([observed_features(category = i[0],feature = i[1]) for i in distinct_features])    
    print 'Finished in %s s' % (time.time() - start)

def get_feature_list(koodous_dbEntry):
    sha256 = koodous_dbEntry.sha256sum
    # sha256 = koodous.objects.get(pk=koodous_id).sha256sum
    # koodousDetailed = koodous.objects.get(pk=koodous_id).json_analysis_detailed
    koodousDetailed = koodous_dbEntry.json_analysis_detailed
    if 'androguard' in koodousDetailed.keys():
        if koodousDetailed['androguard'] is not None:
            androguard = koodousDetailed['androguard']
            classes = []
            methods = []
            if 'functionalities' in androguard.keys():
                for i in androguard['functionalities']:
                    for j in androguard['functionalities'][i]:
                        class_name, method_name = extract_classes_methods(j['code'])
                        if class_name is not None and method_name is not None:
                            classes.append(('class', class_name))
                            methods.append(('method',method_name))
            classes = [list(i) for i in list(set(classes))]
            methods = [list(i) for i in list(set(methods))]
            component_types = ['activities', 'services', 'providers', 'receivers']
            component_list = [androguard[i] for i in component_types]
            components = [['component',i] for sublist in component_list for i in sublist]
            new_permissions = [('permission', i) for i in androguard['new_permissions']]
            permissions = new_permissions + [('permission', i) for i in androguard['permissions']]
            permissions = [list(i) for i in list(set(permissions))]
            try:
                urls = [['url', i] for i in androguard['urls']]
            except KeyError:
                print 'No urls for %s' % sha256
                urls = []
            intents = [['intent', i] for i in androguard['filters'] if ('intent' in i)]
            feature_list = components + permissions + intents + urls + classes + methods
            return feature_list 

    print 'Androguard report does not exist for %s' % sha256
    return []    
    
def extract_classes_methods(codestring):
    try:
        index =  codestring.index(';->')
    except:
        return None, None
    
    method_name = ''
    for i in codestring[index+3:]:
        if i == '(':
            break
        method_name += i
    method_name += '()'
    
    class_name = ''
    for i in reversed(codestring[:index]):
        if i == ' ':
            break
        class_name += i
    class_name =  class_name[::-1]
    method_name = class_name + '.' +method_name
    return class_name, method_name


def feature_list_save(apk_id, feature_list):
    vector_app_features_dbEntry = vector_app_features()
    vector_app_features_dbEntry.apk_id = apk_id
    vector_app_features_dbEntry.bag = [i[1] for i in feature_list]
    vector_app_features_dbEntry.vector = []
    vector_app_features_dbEntry.save()


def flat_feature_save(apk_id, feature_list):
    flat_app_features.objects.bulk_create([flat_app_features(apk_id = apk_id,category = i[0],feature = i[1]) for i in feature_list])


def calculate_feature_vectors():
    print 'Calculating feature vectors'
    start = time.time()
    all_features = observed_features.objects.all().order_by('id').values_list('feature',flat=True)
    all_feature_lists = vector_app_features.objects.all().order_by('apk_id').values_list('bag',flat=True)
    
    for i, app_features in enumerate(all_feature_lists):
        
        if app_features:
            feature_vector = []
            for j, feature in enumerate(all_features):
                feature_vector.append(int(feature in app_features))
            feature_vector_save(i+1,feature_vector)
        else:
            feature_vector = []
            feature_vector_save(i+1,feature_vector)
    print 'Finished in %s s' % (time.time() - start)

def feature_vector_save(apk_id, feature_vector):
    vector_app_features_dbEntry = vector_app_features.objects.get(apk_id=apk_id)
    vector_app_features_dbEntry.vector = feature_vector
    vector_app_features_dbEntry.save()