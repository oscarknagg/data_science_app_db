from django.shortcuts import render
from django.http import JsonResponse
from lib.androlyse import extract_features_from_apk, extract_features_from_apk_quick
from lib.coreLib import get_file_from_s3, calculate_hashes
from lib.dbLib import apk_save_from_androguard
from machineLearning.lib.dbLib import feature_list_save, flat_feature_save
from django.views.decorators.csrf import csrf_exempt
import time


@csrf_exempt
def index(request):
    response = {}
    if request.method == "GET":
        if request.GET:
            print 'API'
        else:
            return render(request, 'androguard/index.html', response)
    else:
        if 'upload' in request.POST.keys():
            print 'Uploading apk from local'
            start = time.time()
            if request.POST['manifestOnly'] == 'true':
                package_name, feature_list = extract_features_from_apk_quick(request.FILES['myfile'])
            elif request.POST['manifestOnly'] == 'false':
                package_name, feature_list = extract_features_from_apk(request.FILES['myfile'])
            else:
                response.update({'error': 'Feature settings not specified.'})
                return JsonResponse(response)
            print 'Analyzed APK in %s' % (time.time() - start)

            response.update({'package_name': package_name})
            response.update({'features': [i[0] + '_' + i[1] for i in feature_list]})

            hashes = calculate_hashes(request.FILES['myfile'])
            response.update({'md5sum': hashes[0]})
            response.update({'sha1sum': hashes[1]})
            response.update({'sha256sum': hashes[2]})

            # apk_db_entry = apk_save_from_androguard(hashes, package_name)
            # if apk_db_entry == -1:
            #     print 'This app is already in the database'
            #     # Add extra methods and classes?
            # else:
            #     print 'Saving features...'
            #     feature_list_save(apk_db_entry.id, feature_list)
            #     flat_feature_save(apk_db_entry.id, feature_list)

        elif 's3' in request.POST.keys():
            print request.POST
            if request.POST['s3filepath'] != '':

                if 'manifestOnly' not in request.POST.keys():
                    response.update({'error': 'Manifest only not specified.'})
                    return JsonResponse(response)

                print 'Getting apk from s3'
                key_file = get_file_from_s3(request.POST['s3filepath'])
                hashes = calculate_hashes(key_file)
                response.update({'md5sum': hashes[0]})
                response.update({'sha1sum': hashes[1]})
                response.update({'sha256sum': hashes[2]})

                start = time.time()
                if request.POST['manifestOnly'] == 'true':
                    package_name, feature_list = extract_features_from_apk_quick(key_file)
                elif request.POST['manifestOnly'] == 'false':
                    package_name, feature_list = extract_features_from_apk(key_file)
                print 'Analyzed APK in %s' % (time.time() - start)

                response.update({'package_name': package_name})
                response.update({'features': [i[0] + '_' + i[1] for i in feature_list]})

                # apk_db_entry = apk_save_from_androguard(hashes, package_name)
                # if apk_db_entry == -1:
                #     print 'This app is already in the database'
                #     # Add extra methods and classes?
                # else:
                #     print 'Saving features...'
                #     feature_list_save(apk_db_entry.id, feature_list)
                #     flat_feature_save(apk_db_entry.id, feature_list)

    return JsonResponse(response)
