import boto, hashlib, boto.s3.connection


def get_file_from_s3(file_path):
    AWS_ACCESS_KEY_ID = 'AKIAIPOOABTWGR6DJXBQ'
    AWS_SECRET_ACCESS_KEY = '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'
    
    ### TODO:
    ### Allow urls to be given as input.
    ### Will have to map first part of url to s3 endpoint using the following docs
    ### http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region

    conn = boto.s3.connect_to_region('eu-west-1',
                                     aws_access_key_id=AWS_ACCESS_KEY_ID,
                                     aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                                     is_secure=True,  # uncomment if you are not using ssl
                                     calling_format=boto.s3.connection.OrdinaryCallingFormat(),
                                     )

    bucket_name = file_path.split('/')[0]
    target_file = '/'.join(file_path.split('/')[1:])

    # bucket_name = 'test-apk-wandera'

    bucket = conn.get_bucket(bucket_name)

    # target_file = 'apk/C6/1A/28/CD/81/C8/81/79/E8/47/BF/12/D3/2E/0F/34/C61A28CD81C88179E847BF12D32E0F34'
    # target_file = 'apk/27/8E/52/3B/16/11/6E/F1/AA/A7/00/12/BA/C5/15/DA/278E523B16116EF1AAA70012BAC515DA'
    # target_file= 'apk/22/C1/ED/B0/B3/71/47/88/DF/58/D2/DD/D9/2E/BB/A5/22C1EDB0B3714788DF58D2DDD92EBBA5'

    key = bucket.get_key(target_file)

    return key


def calculate_hashes(apk_file):
    hashes = []
    for hash_function in ['md5','sha1','sha256']:
        hashes.append(calculate_hash(apk_file, hash_function))
    return tuple(hashes)


def calculate_hash(infile,hash_function):
    blocksize = 65536
    if hash_function == 'md5':
        hasher = hashlib.md5()
    elif hash_function == 'sha1':
        hasher = hashlib.sha1()
    elif hash_function == 'sha256':
        hasher = hashlib.sha256()

    try:
        infile.seek(0)
    except:
        pass

    buf = infile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = infile.read(blocksize)
    return hasher.hexdigest()
