from mobileappanalysis.models import apk
from machineLearning.models import flat_app_features, vector_app_features
from mobileappanalysis.lib.hashlib import isValidMD5 , isValidSHA1 , isValidSHA256


def apk_save_from_androguard(hash_tuple, package_name):
    try:
        if isValidMD5(hash_tuple[0]) is not None:
            entry = apk.objects.get(md5sum=hash_tuple[0])
            return -1
        if isValidSHA1(hash_tuple[1]) is not None:
            entry = apk.objects.get(sha1sum=hash_tuple[1])
            return -1
        if isValidSHA256(hash_tuple[2]) is not None:
            entry = apk.objects.get(sha256sum=hash_tuple[2])
            return -1

    except apk.DoesNotExist:
        apk_db_entry = apk()

        apk_db_entry.md5sum = hash_tuple[0]
        apk_db_entry.sha1sum = hash_tuple[1]
        apk_db_entry.sha256sum = hash_tuple[2]
        apk_db_entry.package_name = package_name

        print 'Saving apk entry'
        apk_db_entry.save()

        return apk_db_entry