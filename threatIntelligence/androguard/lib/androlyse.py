#!/usr/bin/env python

# This file is part of Androguard.
#
# Copyright (C) 2012/2013/2014, Anthony Desnos <desnos at t0t0.fr>
# All rights reserved.
#
# Androguard is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Androguard is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Androguard.  If not, see <http://www.gnu.org/licenses/>.

from androguard.core import *
from androguard.core.androgen import *
from androguard.core.androconf import *
from androguard.core.bytecode import *
from androguard.core.bytecodes.dvm import *
from androguard.core.bytecodes.apk import *

from androguard.core.analysis.analysis import *
from androguard.core.analysis.ganalysis import *
from androguard.core.analysis.risk import *
from androguard.decompiler.decompiler import *

from androguard.util import *
from androguard.misc import *

#from androlib.core import *
#from androlib.core.androgen import *
#from androlib.core.androconf import *
#from androlib.core.bytecode import *
#from androlib.core.bytecodes.dvm import *
#from androglib.core.bytecodes.apk import *
#
#from androlib.core.analysis.analysis import *
#from androlib.core.analysis.ganalysis import *
#from androlib.core.analysis.risk import *
#from androlib.decompiler.decompiler import *
#
#from androlib.util import *
#from androlib.misc import *
#
#from IPython.terminal.embed import InteractiveShellEmbed
#from IPython.config.loader import Config

import xml.dom.minidom


def extract_features(manifest_string,feature_type):
    features = []
    type_dict = {
        'receiver'   : 'receiver',
        'activity'   : 'activity',
        'intent'     : 'action',
        'service'    : 'service',
        'permission' : 'uses-permission'
    }
    substring = '<' + type_dict[feature_type]
    for i in find_substring_indexes(manifest_string,substring):
        j = 0
        scanstring = 'android:name="'
        while True:
            j += 1
            if manifest_string[i+len(substring)+j:i+len(substring)+j+len(scanstring)] == scanstring:
                k = 0
                feature = ''
                while True:
                    feature += manifest_string[i+len(substring)+j+len(scanstring)+k]
                    k += 1
                    if manifest_string[i+len(substring)+j+len(scanstring)+k] == '"':
                        features.append(feature)
                        break
                break
    return list(set(features))


def find_substring_indexes(string, substring):
    start = 0
    while True:
        start = string.find(substring, start)
        if start == -1: return
        yield start
        start += len(substring)


def get_urls(list_of_strings):
    urls = []
    for i in list_of_strings:
        if 'http:' in i or 'https:' in i or 'www' in i:
            urls.append(i)
    return list(set(urls))


def extract_features_from_apk(apk_file):
    print 'Analysing APK...'
    a = APK(apk_file.read(), raw=True)
    print 'Got APK'
    d, dx = AnalyzeDex(a.get_dex(), raw=True, decompiler="dad")
    print 'Got dexes'
    components = a.get_activities() + a.get_services() + a.get_providers() + a.get_receivers()
    print 'Got components'
    permissions = a.get_permissions()
    print 'Got permissions'
    methods = list(set([(i.get_class_name()[:-1] + '.' + i.get_name() + '()') for i in d.get_methods()]))
    print 'Got methods'
    classes = list(set([i.get_class_name()[:-1] for i in d.get_methods()]))
    print 'Got classes'

    manifest = a.get_AndroidManifest()
    manifest_string = manifest.toprettyxml()
    print 'Got manifest'
    intents = extract_features(manifest_string,'intent')
    print 'Got intents'
    
    urls = get_urls(d.get_strings())
    print 'Got urls'
    
    print 'Classes : %s' % len(classes)
    print 'Methods : %s' % len(methods)
    print 'Intents : %s' % len(intents)
    print 'Permissions : %s' % len(permissions)
    print 'Components : %s' % len(components)
    print 'URLS : %s' % len(urls)
    
    feature_list = [('class',i) for i in classes]
    feature_list += [('method',i) for i in methods]
    feature_list += [('intent',i) for i in intents]
    feature_list += [('permission',i) for i in permissions]
    feature_list += [('component',i) for i in components]
    feature_list += [('url',i) for i in urls]

    package_name = a.get_package()
    
    return package_name, feature_list


def extract_features_from_apk_quick(apk_file):
    print 'Analysing APK quickly...'
    a = APK(apk_file.read(), raw=True)
    print 'Got APK'
    components = a.get_activities() + a.get_services() + a.get_providers() + a.get_receivers()
    print 'Got components'
    permissions = a.get_permissions()
    print 'Got permissions'

    manifest = a.get_AndroidManifest()
    manifest_string = manifest.toprettyxml()
    print 'Got manifest'
    intents = extract_features(manifest_string, 'intent')
    print 'Got intents'

    print 'Intents : %s' % len(intents)
    print 'Permissions : %s' % len(permissions)
    print 'Components : %s' % len(components)

    feature_list = [('intent', i) for i in intents]
    feature_list += [('permission', i) for i in permissions]
    feature_list += [('component', i) for i in components]

    package_name = a.get_package()

    return package_name, feature_list