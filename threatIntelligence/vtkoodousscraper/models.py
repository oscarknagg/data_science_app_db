from __future__ import unicode_literals

from django.db import models

# Create your models here.
class search_position(models.Model):
    params =  models.TextField()
    search_URL = models.TextField()
    
    def __str__(self):
        return "%s , %s " % (self.params , self.search_URL)