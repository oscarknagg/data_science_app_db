from vtkoodousscraper.models import search_position
from vtkoodousscraper.lib.dbLib import apk_save, koodous_save, vt_save, link_apk_vt, flat_vt_save
from mobileappanalysis.models import apk, koodous, virustotal
from machineLearning.lib.dbLib import get_feature_list, feature_list_save, flat_feature_save
import json, requests, time, sys, urllib, urllib2, simplejson
import psycopg2
from random import randint


koodous_stop = False
vt_stop = True


def stop_koodous():
    global koodous_stop
    if koodous_stop:
        print 'Koodous search is not running!\n'
    else:
        koodous_stop = True
        'Stopping Koodous search now!\n'


def stop_vt():
    global vt_stop
    if vt_stop:
        print 'Virustotal search is not running!\n'
    else:
        vt_stop = True
        'Stopping Virustotal search now!\n'


def start_koodous(start_type):
    params = {'malign': {'search': 'rating:<0 corrupted:false analyzed:true'},
              'benign': {'search': 'rating:>=0 installed:true corrupted:false analyzed:true'},
              }
    
    # Set up initial search terms if none exist
    for i in params.keys():
        try:
            search_pos_dbEntry = search_position.objects.get(params=params[i])
        except:
            search_pos_dbEntry = search_position()
            search_pos_dbEntry.params = params[i]
            if i == 'malign':
                search_pos_dbEntry.search_URL = 'https://api.koodous.com/apks?search=rating:<0%20corrupted:false%20analyzed:true'
            else:
                search_pos_dbEntry.search_URL = 'https://api.koodous.com/apks?search=rating:>%3D0%20installed:true%20corrupted:false%20analyzed:true'
            search_pos_dbEntry.save()
    
    continue_koodous()
        
        
def continue_koodous():
    params = {'malign': {'search': 'rating:<0 corrupted:false analyzed:true'},
              'benign': {'search': 'rating:>=0 installed:true corrupted:false analyzed:true'},
              }

    global koodous_stop
    koodous_stop = False

    while not koodous_stop:
        kmal = apk.objects.filter(koodous_rating__lt=0).count()
        kbenign = apk.objects.filter(koodous_rating__gte=0).count()
        print '\n\nThere are currently %s malign and  %s benign apps in the database by Koodous rating' % (kmal, kbenign)

        if kmal > kbenign:
            # Get benign batch
            search_pos_dbEntry = search_position.objects.get(params=params['benign'])
            print 'Getting benign apps from search URL %s \n' % (search_pos_dbEntry.search_URL[:50] + '...')
            r = requests.get(url=search_pos_dbEntry.search_URL)
            
            search_pos_dbEntry.search_URL = r.json()['next']
            search_pos_dbEntry.save()
            
            for r in r.json()['results']:
                if koodous_stop:
                    print 'Stopping Koodous search now!'
                    return

                control = save_search_result(r)
                if control == 'break':
                    print 'Entry already exists'
                    break
        else:
            # Get malign batch
            search_pos_dbEntry = search_position.objects.get(params=params['malign'])
            print 'Getting malign apps from search URL %s' % (search_pos_dbEntry.search_URL[:50] + '...')
            r = requests.get(url=search_pos_dbEntry.search_URL)
            
            search_pos_dbEntry.search_URL = r.json()['next']
            search_pos_dbEntry.save()
            
            for r in r.json()['results']:
                if koodous_stop:
                    print 'Stopping Koodous search now!'
                    return

                control = save_search_result(r)
                if control == 'break':
                    print 'Entry already exists'
                    break


def save_search_result(result):
    r = result
    print 'Getting Koodous info for %s' % r['sha256']
    hash_tuple = (r['md5'], r['sha1'], r['sha256'])
    try:
        apk_dbEntry, koodous_dbEntry = analyse_koodous_hash(hash_tuple)
    except TypeError:
        print 'Type Error!'
        print 'apk_id : %s' % apk_dbEntry.id
        print 'koodous_id: %s' % koodous_dbEntry.id
        print 'Hashes: ' + str(hash_tuple)

    if apk_dbEntry == -1 and koodous_dbEntry == -1:
        return 'break'

    feature_list = get_feature_list(koodous_dbEntry)
    feature_list_save(apk_dbEntry.id, feature_list)
    flat_feature_save(apk_dbEntry.id, feature_list)

    return 'pass'


def analyse_koodous_hash(hash_tuple):
    koodous_result = getKoodousResultForSha(hash_tuple[2])
    if len(koodous_result) > 0:
        koodous_dbentry = koodous_save(koodous_result)
        apk_dbEntry, koodous_dbEntry = apk_save(hash_tuple, None, koodous_dbentry)
        return apk_dbEntry, koodous_dbEntry


def getKoodousResultForSha(sha256):
    url_koodous = "https://api.koodous.com/apks/%s" % sha256
    r = requests.get(url=url_koodous)
    try:
        initialAnalysisJson = r.json()
    except:
        return {}
    try:
        if "not found" in initialAnalysisJson["detail"].lower():
            return {}
    except:
        url_koodous = "https://api.koodous.com/apks/%s/analysis" % sha256
        r = requests.get(url=url_koodous)
        try:
            detailedAnalysisJson = r.json()
        except:
            return {}
        return {"initial":initialAnalysisJson,"detailed":detailedAnalysisJson}


def start_virustotal():
    print 'Starting collection of Virustotal labels...'

    global vt_stop
    vt_stop = False

    unlabelled_count = apk.objects.filter(virustotal_id__exact=None).count()
    total_count = apk.objects.count()

    if unlabelled_count*1.0/total_count < 0.1:       # Gets guaranteed unlabelled apps, may not have Androguard features
        unlabelled_apks = apk.objects.filter(virustotal_id__exact=None).order_by('?')[:5760]

        # Add single entry at a time
        # for apk_dbEntry in unlabelled_apks:
        #     if vt_stop:
        #         break
        #     add_vt_single(apk_dbEntry)
        #     interruptible_sleep(15)

        # Add a batch of entries at a time
        apk_list = []
        for apk_dbEntry in unlabelled_apks:
            apk_list.append(apk_dbEntry)
            if len(apk_list) == 4:
                add_vt_batch(apk_list)
                apk_list = []

            for i in range(randint(30, 40)):
                if vt_stop:
                    print 'Stopping virustotal search!'
                    return
                time.sleep(0.5)

    else:                 # Gets unlabelled apps with Androguard report, may be slow if most apps have virustotal labels

        # Add single entry at a time
        # while True:
        #     while True:
        #         apk_dbEntry = get_random_apk_entry_with_features()
        #         if apk_dbEntry.virustotal_id is None:
        #             break
        #
        #     add_vt_single(apk_dbEntry)
        #     interruptible_sleep(15)

        # Add a batch of entries at a time
        while True:
            apk_list = []
            while True:
                apk_dbEntry = get_random_apk_entry_with_features()
                if apk_dbEntry.virustotal_id is None:
                    apk_list.append(apk_dbEntry)
                    if len(apk_list) == 4:
                        break

            add_vt_batch(apk_list)
            for i in range(randint(30, 40)):
                if vt_stop:
                    print 'Stopping virustotal search!'
                    return
                time.sleep(0.5)


def getVTReport(sha256):
    url = "https://www.virustotal.com/vtapi/v2/file/report"
    parameters = {	"resource": sha256,
                  "apikey": "71393ccfaa3b8d53684434b743ebbfae3edfc1b23d3adaabc76e872430cf90e4"}

    try:
        data = urllib.urlencode(parameters)
        req = urllib2.Request(url, data)
        response = urllib2.urlopen(req)
        jsonString = response.read()
        if jsonString is not None:
            result = json.loads(jsonString)
            return result
    except Exception, e:
        print "Unexpected error:", sys.exc_info()[0]
        return None


def add_vt_batch(apk_list):
    print 'There are now %s malign, %s benign and %s unrated apps in the database by Virustotal rating' % (
        apk.objects.filter(vt_detected__gte=5).count(),
        apk.objects.filter(vt_detected__lt=5).count(),
        apk.objects.filter(vt_total__exact=None).count())
    result = getVTReport(', '.join([a.sha256sum for a in apk_list]))
    for i in zip(apk_list, result):
        if i[1] is not None:
            if i[1]['response_code'] is not 0:
                print '%s is malware: %s according to vt, %s according to koodous' % (
                    i[0].sha256sum, i[1]["positives"] > 5, i[0].koodous_rating < 0)
                vt_dbEntry = vt_save(i[1])
                flat_vt_save(vt_dbEntry)
                link_apk_vt(i[0], vt_dbEntry)
            else:
                print 'No vt result for %s' % i[0].sha256sum
        else:
            print 'No vt result for %s' % i[0].sha256sum


def add_vt_single(apk_dbEntry):
    result = getVTReport(apk_dbEntry.sha256sum)
    if result is not None:
        if result['response_code'] is not 0:
            print '%s is malware: %s according to vt, %s according to koodous' % (
                apk_dbEntry.sha256sum, result["positives"] > 5, apk_dbEntry.koodous_rating < 0)
            vt_dbEntry = vt_save(result)
            flat_vt_save(vt_dbEntry)
            link_apk_vt(apk_dbEntry, vt_dbEntry)
        else:
            print 'No vt result for %s' % apk_dbEntry.sha256sum
    else:
        print 'No vt result for %s' % apk_dbEntry.sha256sum


def get_random_apk_entry_with_features():
    while True:
        try:
            conn = psycopg2.connect(
                "dbname='apk' user='apk_analyzer' host='ec2-52-19-94-247.eu-west-1.compute.amazonaws.com' password='r3zyl4n4kp4'")
            cur = conn.cursor()
            cur.execute(""" SELECT CASE WHEN apk_id = 0 THEN 1 ELSE apk_id END
                    FROM
                    (
                    SELECT
                        ROUND(RANDOM() * (SELECT MAX(apk_id) FROM "machineLearning_flat_app_features")) as apk_id
                    ) as r """)
            result= cur.fetchall()
            return apk.objects.get(id=int(result[0][0]))
        except:
            print "I am unable to connect to the database. Trying again in 10s"
            time.sleep(10)
