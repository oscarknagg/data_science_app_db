from mobileappanalysis.models import apk, virustotal, koodous, vt_flat
from mobileappanalysis.lib.vt_koodous import getSophosThreatType
from mobileappanalysis.lib.hashlib import isValidMD5 , isValidSHA1 , isValidSHA256
import json


def vt_save(jsonResult):
    try:
        vt_dbEntry=virustotal.objects.get(sha256sum=jsonResult["sha256"])
        return vt_dbEntry
    except virustotal.DoesNotExist:
        vt_dbEntry = virustotal()
        vt_dbEntry.detected = jsonResult["positives"]
        vt_dbEntry.total = jsonResult["total"]
        vt_dbEntry.sophos_tag = getSophosThreatType(jsonResult)
        vt_dbEntry.json_analysis = jsonResult
        vt_dbEntry.permalink=jsonResult["permalink"]
        vt_dbEntry.sha256sum=jsonResult["sha256"]
        vt_dbEntry.save()
        return vt_dbEntry


def koodous_save(jsonResult):
    initialAnalysis = jsonResult['initial']
    temp = json.dumps(initialAnalysis)
    temp = temp.replace('\u0000','NULL')
    initialAnalysis = json.loads(temp)
    
    initialDetailed = jsonResult['detailed']
    temp = json.dumps(initialDetailed)
    temp = temp.replace('\u0000','NULL')
    initialDetailed = json.loads(temp)
    
    try:
        koodous_dbEntry=koodous.objects.get(sha256sum=initialAnalysis["sha256"])
        return koodous_dbEntry
    except koodous.DoesNotExist:
        koodous_dbEntry = koodous()
        koodous_dbEntry.sha256sum = initialAnalysis["sha256"]
        koodous_dbEntry.rating = initialAnalysis["rating"]
        koodous_dbEntry.tags = initialAnalysis["tags"]
        koodous_dbEntry.json_analysis_initial = initialAnalysis
        koodous_dbEntry.json_analysis_detailed=initialDetailed
        koodous_dbEntry.save()
        return koodous_dbEntry


def apk_save(hash_tuple,virustotalEntry,koodousEntry):
    try:
        if isValidMD5(hash_tuple[0]) is not None:
            entry = apk.objects.get(md5sum=hash_tuple[0])
            return -1, -1
        elif isValidSHA1(hash_tuple[1]) is not None:
            entry = apk.objects.get(sha1sum=hash_tuple[1])
            return -1, -1
        elif isValidSHA256(hash_tuple[2]) is not None:
            entry = apk.objects.get(sha256sum=hash_tuple[2])
            return -1, -1
    
    except apk.DoesNotExist:
        apk_dbEntry = apk()
        
        apk_dbEntry.md5sum = hash_tuple[0]
        apk_dbEntry.sha1sum = hash_tuple[1]
        apk_dbEntry.sha256sum = hash_tuple[2]
        
        if virustotalEntry is not None:
            apk_dbEntry.virustotal_id = virustotalEntry.id
            apk_dbEntry.vt_detected = virustotalEntry.detected
            apk_dbEntry.vt_total = virustotalEntry.total
        
        if koodousEntry is not None:
            apk_dbEntry.package_name = koodousEntry.json_analysis_initial["package_name"]
            apk_dbEntry.dev_name = koodousEntry.json_analysis_initial["company"]
            apk_dbEntry.koodous_displayed_version = koodousEntry.json_analysis_initial["displayed_version"]
            apk_dbEntry.koodous_rating = koodousEntry.json_analysis_initial["rating"]
            apk_dbEntry.koodous_id = koodousEntry.id
        
        apk_dbEntry.save()
        return apk_dbEntry, koodousEntry


def link_apk_vt(apk_dbEntry,vt_dbEntry):
    assert apk_dbEntry.sha256sum == vt_dbEntry.sha256sum, 'Cannot link entries with different sha256sum: %s, %s' % (
        apk_dbEntry.sha256sum, vt_dbEntry.sha256sum)
    apk_dbEntry.virustotal_id = vt_dbEntry.id
    apk_dbEntry.vt_detected = vt_dbEntry.detected
    apk_dbEntry.vt_total = vt_dbEntry.total
    apk_dbEntry.save()


def flat_vt_save(vt_dbEntry):
    vt_flat_dbEntry = vt_flat()
    vt_flat_dbEntry.sha256sum = vt_dbEntry.sha256sum
    vt_flat_dbEntry.virustotal_id = vt_dbEntry.id
    
    j = vt_dbEntry.json_analysis
    scans = j['scans']
    
    if 'ALYac' in scans:
        vt_flat_dbEntry.ALYac_detected = scans['ALYac']['detected']
        if scans['ALYac']['result'] is not None:
            vt_flat_dbEntry.ALYac_result = scans['ALYac']['result'][:200]
        else:
            vt_flat_dbEntry.ALYac_result = None
    if 'AVG' in scans:
        vt_flat_dbEntry.AVG_detected = scans['AVG']['detected']
        if scans['AVG']['result'] is not None:
            vt_flat_dbEntry.AVG_result = scans['AVG']['result'][:200]
        else:
            vt_flat_dbEntry.AVG_result = None
    if 'AVware' in scans:
        vt_flat_dbEntry.AVware_detected = scans['AVware']['detected']
        if scans['AVware']['result'] is not None:
            vt_flat_dbEntry.AVware_result = scans['AVware']['result'][:200]
        else:
            vt_flat_dbEntry.AVware_result = None
    if 'Ad-Aware' in scans:
        vt_flat_dbEntry.AdAware_detected = scans['Ad-Aware']['detected']
        if scans['Ad-Aware']['result'] is not None:
            vt_flat_dbEntry.AdAware_result = scans['Ad-Aware']['result'][:200]
        else:
            vt_flat_dbEntry.AdAware_result = None
    if 'AegisLab' in scans:
        vt_flat_dbEntry.AegisLab_detected = scans['AegisLab']['detected']
        if scans['AegisLab']['result'] is not None:
            vt_flat_dbEntry.AegisLab_result = scans['AegisLab']['result'][:200]
        else:
            vt_flat_dbEntry.AegisLab_result = None
    if 'Agnitum' in scans:
        vt_flat_dbEntry.Agnitum_detected = scans['Agnitum']['detected']
        if scans['Agnitum']['result'] is not None:
            vt_flat_dbEntry.Agnitum_result = scans['Agnitum']['result'][:200]
        else:
            vt_flat_dbEntry.Agnitum_result = None
    if 'AhnLab-V3' in scans:
        vt_flat_dbEntry.AhnLabV3_detected = scans['AhnLab-V3']['detected']
        if scans['AhnLab-V3']['result'] is not None:
            vt_flat_dbEntry.AhnLabV3_result = scans['AhnLab-V3']['result'][:200]
        else:
            vt_flat_dbEntry.AhnLabV3_result = None
    if 'Alibaba' in scans:
        vt_flat_dbEntry.Alibaba_detected = scans['Alibaba']['detected']
        if scans['Alibaba']['result'] is not None:
            vt_flat_dbEntry.Alibaba_result = scans['Alibaba']['result'][:200]
        else:
            vt_flat_dbEntry.Alibaba_result = None
    if 'AntiVir' in scans:
        vt_flat_dbEntry.AntiVir_detected = scans['AntiVir']['detected']
        if scans['AntiVir']['result'] is not None:
            vt_flat_dbEntry.AntiVir_result = scans['AntiVir']['result'][:200]
        else:
            vt_flat_dbEntry.AntiVir_result = None
    if 'Antiy-AVL' in scans:
        vt_flat_dbEntry.AntiyAVL_detected = scans['Antiy-AVL']['detected']
        if scans['Antiy-AVL']['result'] is not None:
            vt_flat_dbEntry.AntiyAVL_result = scans['Antiy-AVL']['result'][:200]
        else:
            vt_flat_dbEntry.AntiyAVL_result = None
    if 'Arcabit' in scans:
        vt_flat_dbEntry.Arcabit_detected = scans['Arcabit']['detected']
        if scans['Arcabit']['result'] is not None:
            vt_flat_dbEntry.Arcabit_result = scans['Arcabit']['result'][:200]
        else:
            vt_flat_dbEntry.Arcabit_result = None
    if 'Avast' in scans:
        vt_flat_dbEntry.Avast_detected = scans['Avast']['detected']
        if scans['Avast']['result'] is not None:
            vt_flat_dbEntry.Avast_result = scans['Avast']['result'][:200]
        else:
            vt_flat_dbEntry.Avast_result = None
    if 'Avira' in scans:
        vt_flat_dbEntry.Avira_detected = scans['Avira']['detected']
        if scans['Avira']['result'] is not None:
            vt_flat_dbEntry.Avira_result = scans['Avira']['result'][:200]
        else:
            vt_flat_dbEntry.Avira_result = None
    if 'Baidu' in scans:
        vt_flat_dbEntry.Baidu_detected = scans['Baidu']['detected']
        if scans['Baidu']['result'] is not None:
            vt_flat_dbEntry.Baidu_result = scans['Baidu']['result'][:200]
        else:
            vt_flat_dbEntry.Baidu_result = None
    if 'Baidu-International' in scans:
        vt_flat_dbEntry.BaiduInternational_detected = scans['Baidu-International']['detected']
        if scans['Baidu-International']['result'] is not None:
            vt_flat_dbEntry.BaiduInternational_result = scans['Baidu-International']['result'][:200]
        else:
            vt_flat_dbEntry.BaiduInternational_result = None
    if 'BitDefender' in scans:
        vt_flat_dbEntry.BitDefender_detected = scans['BitDefender']['detected']
        if scans['BitDefender']['result'] is not None:
            vt_flat_dbEntry.BitDefender_result = scans['BitDefender']['result'][:200]
        else:
            vt_flat_dbEntry.BitDefender_result = None
    if 'Bkav' in scans:
        vt_flat_dbEntry.Bkav_detected = scans['Bkav']['detected']
        if scans['Bkav']['result'] is not None:
            vt_flat_dbEntry.Bkav_result = scans['Bkav']['result'][:200]
        else:
            vt_flat_dbEntry.Bkav_result = None
    if 'ByteHero' in scans:
        vt_flat_dbEntry.ByteHero_detected = scans['ByteHero']['detected']
        if scans['ByteHero']['result'] is not None:
            vt_flat_dbEntry.ByteHero_result = scans['ByteHero']['result'][:200]
        else:
            vt_flat_dbEntry.ByteHero_result = None
    if 'CAT-QuickHeal' in scans:
        vt_flat_dbEntry.CATQuickHeal_detected = scans['CAT-QuickHeal']['detected']
        if scans['CAT-QuickHeal']['result'] is not None:
            vt_flat_dbEntry.CATQuickHeal_result = scans['CAT-QuickHeal']['result'][:200]
        else:
            vt_flat_dbEntry.CATQuickHeal_result = None
    if 'CMC' in scans:
        vt_flat_dbEntry.CMC_detected = scans['CMC']['detected']
        if scans['CMC']['result'] is not None:
            vt_flat_dbEntry.CMC_result = scans['CMC']['result'][:200]
        else:
            vt_flat_dbEntry.CMC_result = None
    if 'ClamAV' in scans:
        vt_flat_dbEntry.ClamAV_detected = scans['ClamAV']['detected']
        if scans['ClamAV']['result'] is not None:
            vt_flat_dbEntry.ClamAV_result = scans['ClamAV']['result'][:200]
        else:
            vt_flat_dbEntry.ClamAV_result = None
    if 'Commtouch' in scans:
        vt_flat_dbEntry.Commtouch_detected = scans['Commtouch']['detected']
        if scans['Commtouch']['result'] is not None:
            vt_flat_dbEntry.Commtouch_result = scans['Commtouch']['result'][:200]
        else:
            vt_flat_dbEntry.Commtouch_result = None
    if 'Comodo' in scans:
        vt_flat_dbEntry.Comodo_detected = scans['Comodo']['detected']
        if scans['Comodo']['result'] is not None:
            vt_flat_dbEntry.Comodo_result = scans['Comodo']['result'][:200]
        else:
            vt_flat_dbEntry.Comodo_result = None
    if 'CrowdStrike' in scans:
        vt_flat_dbEntry.CrowdStrike_detected = scans['CrowdStrike']['detected']
        if scans['CrowdStrike']['result'] is not None:
            vt_flat_dbEntry.CrowdStrike_result = scans['CrowdStrike']['result'][:200]
        else:
            vt_flat_dbEntry.CrowdStrike_result = None
    if 'Cyren' in scans:
        vt_flat_dbEntry.Cyren_detected = scans['Cyren']['detected']
        if scans['Cyren']['result'] is not None:
            vt_flat_dbEntry.Cyren_result = scans['Cyren']['result'][:200]
        else:
            vt_flat_dbEntry.Cyren_result = None
    if 'DrWeb' in scans:
        vt_flat_dbEntry.DrWeb_detected = scans['DrWeb']['detected']
        if scans['DrWeb']['result'] is not None:
            vt_flat_dbEntry.DrWeb_result = scans['DrWeb']['result'][:200]
        else:
            vt_flat_dbEntry.DrWeb_result = None
    if 'ESET-NOD32' in scans:
        vt_flat_dbEntry.ESETNOD32_detected = scans['ESET-NOD32']['detected']
        if scans['ESET-NOD32']['result'] is not None:
            vt_flat_dbEntry.ESETNOD32_result = scans['ESET-NOD32']['result'][:200]
        else:
            vt_flat_dbEntry.ESETNOD32_result = None
    if 'Emsisoft' in scans:
        vt_flat_dbEntry.Emsisoft_detected = scans['Emsisoft']['detected']
        if scans['Emsisoft']['result'] is not None:
            vt_flat_dbEntry.Emsisoft_result = scans['Emsisoft']['result'][:200]
        else:
            vt_flat_dbEntry.Emsisoft_result = None
    if 'F-Prot' in scans:
        vt_flat_dbEntry.FProt_detected = scans['F-Prot']['detected']
        if scans['F-Prot']['result'] is not None:
            vt_flat_dbEntry.FProt_result = scans['F-Prot']['result'][:200]
        else:
            vt_flat_dbEntry.FProt_result = None
    if 'F-Secure' in scans:
        vt_flat_dbEntry.FSecure_detected = scans['F-Secure']['detected']
        if scans['F-Secure']['result'] is not None:
            vt_flat_dbEntry.FSecure_result = scans['F-Secure']['result'][:200]
        else:
            vt_flat_dbEntry.FSecure_result = None
    if 'Fortinet' in scans:
        vt_flat_dbEntry.Fortinet_detected = scans['Fortinet']['detected']
        if scans['Fortinet']['result'] is not None:
            vt_flat_dbEntry.Fortinet_result = scans['Fortinet']['result'][:200]
        else:
            vt_flat_dbEntry.Fortinet_result = None
    if 'GData' in scans:
        vt_flat_dbEntry.GData_detected = scans['GData']['detected']
        if scans['GData']['result'] is not None:
            vt_flat_dbEntry.GData_result = scans['GData']['result'][:200]
        else:
            vt_flat_dbEntry.GData_result = None
    if 'Ikarus' in scans:
        vt_flat_dbEntry.Ikarus_detected = scans['Ikarus']['detected']
        if scans['Ikarus']['result'] is not None:
            vt_flat_dbEntry.Ikarus_result = scans['Ikarus']['result'][:200]
        else:
            vt_flat_dbEntry.Ikarus_result = None
    if 'Invincea' in scans:
        vt_flat_dbEntry.Invincea_detected = scans['Invincea']['detected']
        if scans['Invincea']['result'] is not None:
            vt_flat_dbEntry.Invincea_result = scans['Invincea']['result'][:200]
        else:
            vt_flat_dbEntry.Invincea_result = None
    if 'Jiangmin' in scans:
        vt_flat_dbEntry.Jiangmin_detected = scans['Jiangmin']['detected']
        if scans['Jiangmin']['result'] is not None:
            vt_flat_dbEntry.Jiangmin_result = scans['Jiangmin']['result'][:200]
        else:
            vt_flat_dbEntry.Jiangmin_result = None
    if 'K7AntiVirus' in scans:
        vt_flat_dbEntry.K7AntiVirus_detected = scans['K7AntiVirus']['detected']
        if scans['K7AntiVirus']['result'] is not None:
            vt_flat_dbEntry.K7AntiVirus_result = scans['K7AntiVirus']['result'][:200]
        else:
            vt_flat_dbEntry.K7AntiVirus_result = None
    if 'K7GW' in scans:
        vt_flat_dbEntry.K7GW_detected = scans['K7GW']['detected']
        if scans['K7GW']['result'] is not None:
            vt_flat_dbEntry.K7GW_result = scans['K7GW']['result'][:200]
        else:
            vt_flat_dbEntry.K7GW_result = None
    if 'Kaspersky' in scans:
        vt_flat_dbEntry.Kaspersky_detected = scans['Kaspersky']['detected']
        if scans['Kaspersky']['result'] is not None:
            vt_flat_dbEntry.Kaspersky_result = scans['Kaspersky']['result'][:200]
        else:
            vt_flat_dbEntry.Kaspersky_result = None
    if 'Kingsoft' in scans:
        vt_flat_dbEntry.Kingsoft_detected = scans['Kingsoft']['detected']
        if scans['Kingsoft']['result'] is not None:
            vt_flat_dbEntry.Kingsoft_result = scans['Kingsoft']['result'][:200]
        else:
            vt_flat_dbEntry.Kingsoft_result = None
    if 'Malwarebytes' in scans:
        vt_flat_dbEntry.Malwarebytes_detected = scans['Malwarebytes']['detected']
        if scans['Malwarebytes']['result'] is not None:
            vt_flat_dbEntry.Malwarebytes_result = scans['Malwarebytes']['result'][:200]
        else:
            vt_flat_dbEntry.Malwarebytes_result = None
    if 'McAfee' in scans:
        vt_flat_dbEntry.McAfee_detected = scans['McAfee']['detected']
        if scans['McAfee']['result'] is not None:
            vt_flat_dbEntry.McAfee_result = scans['McAfee']['result'][:200]
        else:
            vt_flat_dbEntry.McAfee_result = None
    if 'McAfee-GW-Edition' in scans:
        vt_flat_dbEntry.McAfeeGWEdition_detected = scans['McAfee-GW-Edition']['detected']
        if scans['McAfee-GW-Edition']['result'] is not None:
            vt_flat_dbEntry.McAfeeGWEdition_result = scans['McAfee-GW-Edition']['result'][:200]
        else:
            vt_flat_dbEntry.McAfeeGWEdition_result = None
    if 'MicroWorld-eScan' in scans:
        vt_flat_dbEntry.MicroWorldeScan_detected = scans['MicroWorld-eScan']['detected']
        if scans['MicroWorld-eScan']['result'] is not None:
            vt_flat_dbEntry.MicroWorldeScan_result = scans['MicroWorld-eScan']['result'][:200]
        else:
            vt_flat_dbEntry.MicroWorldeScan_result = None
    if 'Microsoft' in scans:
        vt_flat_dbEntry.Microsoft_detected = scans['Microsoft']['detected']
        if scans['Microsoft']['result'] is not None:
            vt_flat_dbEntry.Microsoft_result = scans['Microsoft']['result'][:200]
        else:
            vt_flat_dbEntry.Microsoft_result = None
    if 'NANO-Antivirus' in scans:
        vt_flat_dbEntry.NANOAntivirus_detected = scans['NANO-Antivirus']['detected']
        if scans['NANO-Antivirus']['result'] is not None:
            vt_flat_dbEntry.NANOAntivirus_result = scans['NANO-Antivirus']['result'][:200]
        else:
            vt_flat_dbEntry.NANOAntivirus_result = None
    if 'Norman' in scans:
        vt_flat_dbEntry.Norman_detected = scans['Norman']['detected']
        if scans['Norman']['result'] is not None:
            vt_flat_dbEntry.Norman_result = scans['Norman']['result'][:200]
        else:
            vt_flat_dbEntry.Norman_result = None
    if 'PCTools' in scans:
        vt_flat_dbEntry.PCTools_detected = scans['PCTools']['detected']
        if scans['PCTools']['result'] is not None:
            vt_flat_dbEntry.PCTools_result = scans['PCTools']['result'][:200]
        else:
            vt_flat_dbEntry.PCTools_result = None
    if 'Panda' in scans:
        vt_flat_dbEntry.Panda_detected = scans['Panda']['detected']
        if scans['Panda']['result'] is not None:
            vt_flat_dbEntry.Panda_result = scans['Panda']['result'][:200]
        else:
            vt_flat_dbEntry.Panda_result = None
    if 'Qihoo-360' in scans:
        vt_flat_dbEntry.Qihoo360_detected = scans['Qihoo-360']['detected']
        if scans['Qihoo-360']['result'] is not None:
            vt_flat_dbEntry.Qihoo360_result = scans['Qihoo-360']['result'][:200]
        else:
            vt_flat_dbEntry.Qihoo360_result = None
    if 'Rising' in scans:
        vt_flat_dbEntry.Rising_detected = scans['Rising']['detected']
        if scans['Rising']['result'] is not None:
            vt_flat_dbEntry.Rising_result = scans['Rising']['result'][:200]
        else:
            vt_flat_dbEntry.Rising_result = None
    if 'SUPERAntiSpyware' in scans:
        vt_flat_dbEntry.SUPERAntiSpyware_detected = scans['SUPERAntiSpyware']['detected']
        if scans['SUPERAntiSpyware']['result'] is not None:
            vt_flat_dbEntry.SUPERAntiSpyware_result = scans['SUPERAntiSpyware']['result'][:200]
        else:
            vt_flat_dbEntry.SUPERAntiSpyware_result = None
    if 'Sophos' in scans:
        vt_flat_dbEntry.Sophos_detected = scans['Sophos']['detected']
        if scans['Sophos']['result'] is not None:
            vt_flat_dbEntry.Sophos_result = scans['Sophos']['result'][:200]
        else:
            vt_flat_dbEntry.Sophos_result = None
    if 'Symantec' in scans:
        vt_flat_dbEntry.Symantec_detected = scans['Symantec']['detected']
        if scans['Symantec']['result'] is not None:
            vt_flat_dbEntry.Symantec_result = scans['Symantec']['result'][:200]
        else:
            vt_flat_dbEntry.Symantec_result = None
    if 'Tencent' in scans:
        vt_flat_dbEntry.Tencent_detected = scans['Tencent']['detected']
        if scans['Tencent']['result'] is not None:
            vt_flat_dbEntry.Tencent_result = scans['Tencent']['result'][:200]
        else:
            vt_flat_dbEntry.Tencent_result = None
    if 'TheHacker' in scans:
        vt_flat_dbEntry.TheHacker_detected = scans['TheHacker']['detected']
        if scans['TheHacker']['result'] is not None:
            vt_flat_dbEntry.TheHacker_result = scans['TheHacker']['result'][:200]
        else:
            vt_flat_dbEntry.TheHacker_result = None
    if 'TotalDefense' in scans:
        vt_flat_dbEntry.TotalDefense_detected = scans['TotalDefense']['detected']
        if scans['TotalDefense']['result'] is not None:
            vt_flat_dbEntry.TotalDefense_result = scans['TotalDefense']['result'][:200]
        else:
            vt_flat_dbEntry.TotalDefense_result = None
    if 'TrendMicro' in scans:
        vt_flat_dbEntry.TrendMicro_detected = scans['TrendMicro']['detected']
        if scans['TrendMicro']['result'] is not None:
            vt_flat_dbEntry.TrendMicro_result = scans['TrendMicro']['result'][:200]
        else:
            vt_flat_dbEntry.TrendMicro_result = None
    if 'TrendMicro-HouseCall' in scans:
        vt_flat_dbEntry.TrendMicroHouseCall_detected = scans['TrendMicro-HouseCall']['detected']
        if scans['TrendMicro-HouseCall']['result'] is not None:
            vt_flat_dbEntry.TrendMicroHouseCall_result = scans['TrendMicro-HouseCall']['result'][:200]
        else:
            vt_flat_dbEntry.TrendMicroHouseCall_result = None
    if 'VBA32' in scans:
        vt_flat_dbEntry.VBA32_detected = scans['VBA32']['detected']
        if scans['VBA32']['result'] is not None:
            vt_flat_dbEntry.VBA32_result = scans['VBA32']['result'][:200]
        else:
            vt_flat_dbEntry.VBA32_result = None
    if 'VIPRE' in scans:
        vt_flat_dbEntry.VIPRE_detected = scans['VIPRE']['detected']
        if scans['VIPRE']['result'] is not None:
            vt_flat_dbEntry.VIPRE_result = scans['VIPRE']['result'][:200]
        else:
            vt_flat_dbEntry.VIPRE_result = None
    if 'ViRobot' in scans:
        vt_flat_dbEntry.ViRobot_detected = scans['ViRobot']['detected']
        if scans['ViRobot']['result'] is not None:
            vt_flat_dbEntry.ViRobot_result = scans['ViRobot']['result'][:200]
        else:
            vt_flat_dbEntry.ViRobot_result = None
    if 'Yandex' in scans:
        vt_flat_dbEntry.Yandex_detected = scans['Yandex']['detected']
        if scans['Yandex']['result'] is not None:
            vt_flat_dbEntry.Yandex_result = scans['Yandex']['result'][:200]
        else:
            vt_flat_dbEntry.Yandex_result = None
    if 'Zillya' in scans:
        vt_flat_dbEntry.Zillya_detected = scans['Zillya']['detected']
        if scans['Zillya']['result'] is not None:
            vt_flat_dbEntry.Zillya_result = scans['Zillya']['result'][:200]
        else:
            vt_flat_dbEntry.Zillya_result = None
    if 'Zoner' in scans:
        vt_flat_dbEntry.Zoner_detected = scans['Zoner']['detected']
        if scans['Zoner']['result'] is not None:
            vt_flat_dbEntry.Zoner_result = scans['Zoner']['result'][:200]
        else:
            vt_flat_dbEntry.Zoner_result = None
    if 'ahnlab' in scans:
        vt_flat_dbEntry.ahnlab_detected = scans['ahnlab']['detected']
        if scans['ahnlab']['result'] is not None:
            vt_flat_dbEntry.ahnlab_result = scans['ahnlab']['result'][:200]
        else:
            vt_flat_dbEntry.ahnlab_result = None
    if 'eSafe' in scans:
        vt_flat_dbEntry.eSafe_detected = scans['eSafe']['detected']
        if scans['eSafe']['result'] is not None:
            vt_flat_dbEntry.eSafe_result = scans['eSafe']['result'][:200]
        else:
            vt_flat_dbEntry.eSafe_result = None
    if 'nProtect' in scans:
        vt_flat_dbEntry.nProtect_detected = scans['nProtect']['detected']
        if scans['nProtect']['result'] is not None:
            vt_flat_dbEntry.nProtect_result = scans['nProtect']['result'][:200]
        else:
            vt_flat_dbEntry.nProtect_result = None
    
    vt_flat_dbEntry.save()
        
    
    
    
    
    
    
    
    
    
    
    