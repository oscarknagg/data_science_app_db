from django.shortcuts import render
from mobileappanalysis.models import apk
from lib.coreLib import start_koodous, start_virustotal, stop_koodous, stop_vt


# Create your views here.
def index(request):
    response = {}
    
    response.update({"vmal": apk.objects.filter(vt_detected__gte=5).count()})
    response.update({"vbenign": apk.objects.filter(vt_detected__lt=5).count()})
    response.update({"vnull": apk.objects.filter(vt_detected__exact=None).count()})
    
    kmal = apk.objects.filter(koodous_rating__lt=0).count()
    kbenign = apk.objects.filter(koodous_rating__gte=0).count()
    response.update({"kmal": kmal})
    response.update({"kbenign": kbenign})
    
    if request.method == "POST":
        if 'koodousbegin' in request.POST.keys():
            if kmal > kbenign:
                start_koodous('benign')
            else:
                start_koodous('malign')
        elif 'koodousstop' in request.POST.keys():
            stop_koodous()
        elif 'vtbegin' in request.POST.keys():
            start_virustotal()
        elif 'vtstop' in request.POST.keys():
            stop_vt()

    return render(request, 'vtkoodousscraper/index.html', response)

