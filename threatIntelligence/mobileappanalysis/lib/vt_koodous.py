import requests
import urllib
import json
import sys
import time
from random import randint
import re
import simplejson
import urllib2
import pprint
import os

'''
	Usage : 
	python VT_koodousReport.py /path/to/File/Containing/Hashes
	Generates initially VirusTotalReport.txt and then Combined VirusTotal and Koodous Report named VT_Koodous.txt
	After the end of the Script, put all the analysis.txt files in a folder called json 
	This will help the rest of the scripts to operate normally
'''

def getDetailsForSha(searchTerm):
	if '-' in searchTerm:
		return json.loads('{"next":null,"previous":null,"results":[],"count":12319926}')
	url_koodous = "https://api.koodous.com/apks?"
	if isValidSHA256(searchTerm):
		url_koodous += "sha256=%s" % searchTerm
	elif isValidMD5(searchTerm):
		url_koodous += "md5=%s" % searchTerm
	elif isValidSHA1(searchTerm):
		url_koodous += "sha1=%s" % searchTerm
	else:
		url_koodous += "md5=%s" % searchTerm
	#print url_koodous
	r = requests.get(url=url_koodous)
	detailsJson = r.json()
	#print json.dumps(r.json(), sort_keys=True,indent=4, separators=(',', ': '))
	return detailsJson

def getAnalysisForSha(sha256):
	url_koodous = "https://api.koodous.com/apks/%s/analysis" % sha256
	r = requests.get(url=url_koodous)
	analysisJson = r.json()
	#print json.dumps(r.json(), sort_keys=True,indent=4, separators=(',', ': '))
	return analysisJson


def isValidSHA1(string):
    return re.search(r'[a-fA-F0-9]{40}',str(string).strip())


def isValidMD5(string):
    return re.search(r'[a-fA-F0-9]{32}',str(string).strip())


def isValidSHA256(string):
    return re.search(r'[a-fA-F0-9]{64}',str(string).strip())

def getVTReport(sha256):
	url = "https://www.virustotal.com/vtapi/v2/file/report"
	parameters = {	"resource": sha256,
	 				"apikey": "d28cf2fee601a4edff786ce902046b4efc6b634d3f94ab9043ce33378089d7de"}
	try:
		data = urllib.urlencode(parameters)
		req = urllib2.Request(url, data)
		response = urllib2.urlopen(req)
		jsonString = response.read()
		if jsonString is not None:
			result = json.loads(jsonString)
			return result
	except  Exception, e:
		print "Unexpected error:", sys.exc_info()[0]
		return None

def getDetections(jsonVTReport):
	return "["+str(jsonVTReport["positives"])+'/'+str(jsonVTReport["total"])+"]"

def getSophosThreatType(jsonReport):
	try:
		return str(jsonReport["scans"]["Sophos"]["result"])
	except  Exception, e:
		print "Error with jsonReport" + str(e)
		return 'None'



if __name__ == '__main__':
	
	filePath=sys.argv[1]

	if not os.path.isfile('VirusTotalReport.txt'):
		file = open('VirusTotalReport.txt','w+')
		file.close()

	if not os.path.isfile('VT_Koodous.txt'):
		file = open('VT_Koodous.txt','w+')
		file.close()

	with open(filePath,'r') as inputFile:
		hashes = inputFile.readlines()

	with open('VirusTotalReport.txt','r') as virusTotalReport:
		log = virusTotalReport.readlines()
	
	with open('VirusTotalReport.txt','a+') as virusTotalReport:
		print "Output File : VirusTotalReport.txt"
		virusTotalReport.write("SearchTerm,MD5,SHA1,SHA256,Detections,SophosThreatType\n")
		
		for searchTerm in hashes:
			searchTerm = searchTerm.strip()

			if str(searchTerm).strip() in str(log):	
				continue
			
			result = getVTReport(searchTerm)

			if result['response_code'] is not 0:
				#pprint.pprint(result)
				virusTotalReport.write(str(searchTerm).strip()+',')
				virusTotalReport.write(str(result["md5"]).strip()+',')
				virusTotalReport.write(str(result["sha1"]).strip()+',')
				virusTotalReport.write(str(result["sha256"]).strip()+',')
				virusTotalReport.write(str(getDetections(result)).strip()+',')
				virusTotalReport.write(str(getSophosThreatType(result)).strip()+'\n')

				print searchTerm+' '+str(result["md5"]).strip()+" "+str(result["sha1"]).strip()+" "+str(result["sha256"]).strip()+" "+getDetections(result).strip()+" "+getSophosThreatType(result).strip()
				time.sleep(randint(15,25))
			else:
				time.sleep(randint(15,25))
				if isValidSHA256(searchTerm):
					virusTotalReport.write(str(searchTerm).strip()+",")
					virusTotalReport.write("-,")
					virusTotalReport.write("-,")
					virusTotalReport.write(str(searchTerm).strip()+",")
					virusTotalReport.write("-,")
					virusTotalReport.write("-\n")
				elif isValidMD5(searchTerm):
					virusTotalReport.write(str(searchTerm).strip()+",")
					virusTotalReport.write(str(searchTerm).strip()+",")
					virusTotalReport.write("-,")
					virusTotalReport.write("-,")
					virusTotalReport.write("-,")
					virusTotalReport.write("-\n")
				elif isValidSHA1(searchTerm):
					virusTotalReport.write(str(searchTerm).strip()+",")
					virusTotalReport.write("-,")
					virusTotalReport.write(str(searchTerm).strip()+",")
					virusTotalReport.write("-,")
					virusTotalReport.write("-,")
					virusTotalReport.write("-\n")
				else:
					virusTotalReport.write(str(searchTerm).strip()+",")
					virusTotalReport.write("-,")
					virusTotalReport.write("-,")
					virusTotalReport.write("-,")
					virusTotalReport.write("-,")
					virusTotalReport.write("-\n")


	with open('VirusTotalReport.txt','r') as virusTotalReport:
		lines = virusTotalReport.readlines()

	with open('VT_Koodous.txt','r') as analysed:
		analysedLines = analysed.readlines()

	with open('VT_Koodous.txt','a+') as final:
		final.write(str("SearchTerm,MD5,SHA1,SHA256,VT_Result,Sophos_Tag,K_app_name,K_package_name,K_developer,K_image,K_tags"+"\n"))
		print str("SearchTerm,MD5,SHA1,SHA256,VT_Result,Sophos_Tag,K_app_name,K_package_name,K_developer,K_image,K_tags")

		for i in lines:

			if "MD5,SHA1,SHA256,Detections" in i or str(i).strip() in str(analysedLines):
				continue


			values = i.strip().split(',')
			searchTerm = values[0].strip()
			md5 = values[1].strip()
			sha1 = values[2].strip()
			sha256=values[3].strip()
			detections = values[4].strip()
			sophosTag =values[5].strip()
			final.write(str(searchTerm+','+md5+','+sha1+','+sha256+','+detections+','+sophosTag+','))
			hashList=[]
			hashList.append(md5)
			hashList.append(sha1)
			hashList.append(sha256)

			for eachHash in hashList:
				details=getDetailsForSha(eachHash)			
				details=details["results"]
				if len(details)>0:
					break
			
			if len(details)>0:
				
				if not "Not found" in str(details):
					details=details[0]
					analyzed = str(details["analyzed"]).strip()
					app_name = str(details["app"].encode('utf-8')).strip()
					package_name = str(details["package_name"]).strip()
					developer = str(details["company"]).strip()
					imageLink = str(details["image"]).strip()
					tags = details["tags"]
					if len(tags)>0:
						tagArray='('
						for tag in tags:
							tagArray+=tag+'|'
						tagArray=tagArray[:-1]+')'
					else:
						tagArray ='()'

					
					final.write(app_name+','+package_name+','+developer+','+imageLink+','+str(tagArray)+'\n')
					print(str(md5+','+sha256+','+detections+','+sophosTag+',')+app_name+','+package_name+','+developer+','+imageLink+','+str(tagArray))

					if analyzed:

						analysis = getAnalysisForSha(sha256)
						with open(package_name+'_'+sha256+'_analysis.txt','w+') as outputFile:
							outputFile.write(json.dumps(analysis, sort_keys=True,indent=4, separators=(',', ': ')))
					else:
						with open(package_name+'_'+sha256+'_analysis.txt','w+') as outputFile:
							outputFile.write('NOT FOUND')
				else:
					final.write(str('NOT FOUND,-,-,-,-\n'))
			else:
					final.write(str('NOT FOUND,-,-,-,-\n'))
