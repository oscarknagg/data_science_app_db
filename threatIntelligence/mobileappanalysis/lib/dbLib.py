from mobileappanalysis.models import apk, virustotal, koodous
from mobileappanalysis.lib.vt_koodous import getSophosThreatType
from mobileappanalysis.lib.hashlib import isValidMD5 , isValidSHA1 , isValidSHA256
import json

def hashExists(hash):
	return False

def vt_save(jsonResult):
	try:
		vt_dbEntry=virustotal.objects.get(sha256sum=jsonResult["sha256"])
		return vt_dbEntry
	except virustotal.DoesNotExist:
		vt_dbEntry = virustotal()
		vt_dbEntry.detected = jsonResult["positives"]
		vt_dbEntry.total = jsonResult["total"]
		vt_dbEntry.sophos_tag = getSophosThreatType(jsonResult)
		vt_dbEntry.json_analysis = jsonResult
		vt_dbEntry.permalink=jsonResult["permalink"]
		vt_dbEntry.sha256sum=jsonResult["sha256"]
		vt_dbEntry.save()
		return vt_dbEntry


def koodous_save(jsonResult):
	initialAnalysis = jsonResult['initial']
	initialDetailed = jsonResult['detailed']
	temp = json.dumps(initialDetailed)
	temp = temp.replace('\u0000','NULL')
	initialDetailed = json.loads(temp)

	print "Checking if  %s PREEXISTS" %initialAnalysis["sha256"]
	try:
		koodous_dbEntry=koodous.objects.get(sha256sum=initialAnalysis["sha256"])
		return koodous_dbEntry
	except koodous.DoesNotExist:
		koodous_dbEntry = koodous()
		koodous_dbEntry.sha256sum = initialAnalysis["sha256"]
		koodous_dbEntry.rating = initialAnalysis["rating"]
		koodous_dbEntry.tags = initialAnalysis["tags"]
		koodous_dbEntry.json_analysis_initial = initialAnalysis
		koodous_dbEntry.json_analysis_detailed=initialDetailed
		koodous_dbEntry.save()
		return koodous_dbEntry

def apk_save(searchTerm,virustotalEntry,koodousEntry):
	try:
		if isValidMD5(searchTerm) is not None:
			entry = apk.objects.get(md5sum=searchTerm)
			return entry	
		elif isValidSHA1(searchTerm) is not None:
			entry = apk.objects.get(sha1sum=searchTerm)
			return entry
		elif isValidSHA256(searchTerm) is not None:
			entry = apk.objects.get(sha256sum=searchTerm)
			return entry
	except apk.DoesNotExist:
		apkDbEntry=apk()

		if virustotalEntry is not None:
			apkDbEntry.virustotal_id= virustotalEntry.id
			apkDbEntry.vt_detected=virustotalEntry.detected
			apkDbEntry.vt_total=virustotalEntry.total
			apkDbEntry.md5sum=virustotalEntry.json_analysis["md5"]
			apkDbEntry.sha1sum=virustotalEntry.json_analysis["sha1"]
			apkDbEntry.sha256sum=virustotalEntry.json_analysis["sha256"]


		if koodousEntry is not None:
			apkDbEntry.package_name = koodousEntry.json_analysis_initial["package_name"]
			apkDbEntry.dev_name = koodousEntry.json_analysis_initial["company"]
			apkDbEntry.koodous_displayed_version = koodousEntry.json_analysis_initial["displayed_version"]
			apkDbEntry.koodous_rating = koodousEntry.json_analysis_initial["rating"]
			apkDbEntry.koodous_id= koodousEntry.id

		if isValidMD5(searchTerm) is not None:
			apkDbEntry.md5sum=searchTerm
		elif isValidSHA1(searchTerm) is not None:
			apkDbEntry.sha1sum=searchTerm
		elif isValidSHA256(searchTerm) is not None:
			apkDbEntry.sha256sum=searchTerm
		
		apkDbEntry.save()
