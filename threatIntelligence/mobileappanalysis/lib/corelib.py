from mobileappanalysis.lib.hashlib import isValidMD5,isValidSHA256,isValidSHA1
from mobileappanalysis.lib.vt_koodous import getVTReport,getDetections,getSophosThreatType
from mobileappanalysis.models import apk,virustotal
from dbLib import vt_save, koodous_save, apk_save
import pprint
import threading
import time
from random import randint
import requests

def runHashAnalysis(listAsString):
	hashList= getListFromString(listAsString)
	invalidHashesList = []
	for searchTerm in hashList:
		searchTerm = searchTerm.strip().encode('ascii')
		print "Checking : "+str(searchTerm)
		analyseHash(searchTerm)	
			

def analyseHash(searchTerm):
	'''
	if isValidSHA256(searchTerm) is not None:
		result = getVTReport(searchTerm)
		koodousResult = getKoodousResultForSha(searchTerm)
		if result is not None:
			if result['response_code'] is not 0:
	'''



	result = getVTReport(searchTerm)
	if result is not None:
		if result['response_code'] is not 0:
			vt_dbEntry = vt_save(result)
			koodousResult = getKoodousResultForSha(result["sha256"])
			if len(koodousResult)>0:
				koodous_dbentry = koodous_save(koodousResult)
				apk_save(searchTerm,vt_dbEntry,koodous_dbentry)
			else:
				apk_save(searchTerm,vt_dbEntry,None)
		else:
			print "Sample has not been seen submitted on VirusTotal"
			if isValidSHA256(searchTerm) is not None:
				koodousResult = getKoodousResultForSha(searchTerm)
				if len(koodousResult)>0:
					koodous_dbentry = koodous_save(koodousResult)
					apk_save(searchTerm,None,koodous_dbentry)
				else:
					apk_save(searchTerm,None,None)
					print "No VirusTotal or Koodous Results regarding %s"  %searchTerm
	else:
		if isValidSHA256(searchTerm) is not None:
			koodousResult = getKoodousResultForSha(searchTerm)
			if len(koodousResult)>0:
				koodous_dbentry = koodous_save(koodousResult)
				apk_save(searchTerm,None,koodous_dbentry)
			else:
				apk_save(searchTerm,None,None)
				print "No VirusTotal or Koodous Results regarding %s"  %searchTerm
		else:
			print "No VirusTotal and not a SHA256 hash which would enable Koodous to provide results for %s"  %searchTerm
	time.sleep(randint(15,25))

def getKoodousResultForSha(sha256):
	url_koodous = "https://api.koodous.com/apks/%s" % sha256
	r = requests.get(url=url_koodous)
	initialAnalysisJson = r.json()
	try:
		if "not found" in initialAnalysisJson["detail"].lower():
			return {}
	except:
		
		url_koodous = "https://api.koodous.com/apks/%s/analysis" % sha256
		r = requests.get(url=url_koodous)
		detailedAnalysisJson = r.json()
		return {"initial":initialAnalysisJson,"detailed":detailedAnalysisJson}


def checkExistingHashes(listAsString):
	hashList = getListFromString(listAsString)
	results=[]
	toBeChecked = []
	for value in hashList:
		value = value.strip().encode('ascii')
		try:
			if isValidMD5(value) is not None:
				listing = apk.objects.get(md5sum=value)
				results.append(listing)
			elif isValidSHA1(value) is not None:
				listing = apk.objects.get(sha1sum=value)
				results.append(listing)	
			elif isValidSHA256(value) is not None:
				listing = apk.objects.get(sha256sum=value)
				results.append(listing)
			else:
				print "Not valid Hash"
		except apk.DoesNotExist:
			toBeChecked.append(value)
	return {"existing" : results,"toBeChecked":toBeChecked}

def getListFromString(listAsString):
	if type(listAsString) is list:
		return listAsString
	else:
		if '\n' in listAsString:
			return listAsString.split('\n')
		else:
			temp = []
			temp.append(listAsString)
			return temp

def runUrlAnalysis(listAsString):
	for item in listAsString.split(' '):
		print item
	return listAsString