import re

def isValidSHA1(string):
    return re.search(r'^[a-fA-F0-9]{40}$',str(string).strip())

def isValidMD5(string):
    return re.search(r'^[a-fA-F0-9]{32}$',str(string).strip())


def isValidSHA256(string):
    return re.search(r'^[a-fA-F0-9]{64}$',str(string).strip())

def isValidHash(string):
	return re.search(r'^[a-fA-F0-9]{32,64}$',str(string).strip())