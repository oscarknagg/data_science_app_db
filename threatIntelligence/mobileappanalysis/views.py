from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from lib.vt_koodous import *
from models import *
from lib.hashlib import isValidHash
from lib.corelib import runHashAnalysis,runUrlAnalysis,checkExistingHashes
import threading

# Create your views here.
def index(request):

	response = {}
	if request.method =="GET":
		return render(request, 'mobileappanalysis/index.html', response)
	else:
			hashes = request.POST['hashes']
			urls = request.POST['urls']
			if not urls and not hashes:
				Error_Message="No Urls or hashes To Check"
				response.update({"error":Error_Message})
				return render(request, 'mobileappanalysis/index.html', response)
			if hashes:
				finalResults = checkExistingHashes(hashes)
				response.update({"virustotal":finalResults["existing"]})
				
				t = threading.Thread(target=runHashAnalysis,
                            args=(finalResults["toBeChecked"],))
				t.setDaemon(True)
				t.start()
	    
				'''
				if urls:
					runUrlAnalysis(urls)
					response.update({"virustotal":urls})	
				'''
	return render(request, 'mobileappanalysis/index.html', response)